import './App.css';
import {Switch, Route, Redirect, NavLink} from 'react-router-dom';
import PokemonList from './containers/PokemonList';
import Pokemon from './containers/Pokemon';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  navBar: {
    backgroundColor: 'crimson',
    fontWeight: 'bold',
    color: 'whitesmoke',
    height: '10vh',
    width: '100%',
    margin: 0,
    padding: 0,
    position: 'fixed',
    textDecoration: 'none'
  }
})

function App() {

  const classes = useStyles();

  return (
    <div>
      <nav className={classes.navBar}>
        <NavLink to={'/'}>Pokedex</NavLink>
      </nav>
      <Switch>
        <Route path={'/'} exact component={PokemonList} />
        <Route path={'/pokemon/:pokemon'} exact component={Pokemon} />
        <Redirect to={'/'} />
      </Switch>
    </div>
  );
}

export default App;
