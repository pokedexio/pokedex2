import React, { useEffect } from 'react';
import { GetPokemon } from '../actions/pokemonActions';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import _ from 'lodash';

const useStyles = makeStyles({
  mainDiv: {
    padding: 90,

  },
  imgPokemon: {
    height: 300
  },
  namePokemon: {
    textTransform: 'capitalize'
  }
})

const Pokemon = (props) => {
  
  const classes = useStyles();

  const pokemonName = props.match.params.pokemon
  const dispatch = useDispatch();
  const pokemonState = useSelector(state => state.Pokemon);
  
  useEffect(() => {
    dispatch(GetPokemon(pokemonName));
  })

  const ShowData = () => {
    if (!_.isEmpty(pokemonState.data[pokemonName])){

      const pokeData = pokemonState.data[pokemonName];

      return (
        <div>
          <div>
            <img src={pokeData.sprites.front_default} alt={pokeData.name} className={classes.imgPokemon} />
          </div>
          <div>
            <h1 className={classes.namePokemon}>Stats</h1>
            {pokeData.stats.map(el => {
              return <p>{el.stat.name} {el.base_stat}</p>
            })}
          </div>
          <div>
            <h1>Abilities</h1>
            {pokeData.abilities.map(el => {
              return <p>{el.ability.name}</p>
            })}
          </div>
        </div>
      )
    }

    if(pokemonState.loading){
      <p>Loading...</p>
    }

    if(pokemonState.errorMsg !== ""){
      return <p>{pokemonState.errorMsg }</p>
    }

    return <p>No Pokemon Data</p>

  }

  return (
    <div className={classes.mainDiv}>
        <h1 className={classes.namePokemon}>{pokemonName}</h1>
        {ShowData()}
    </div>
  );
}

export default Pokemon;
