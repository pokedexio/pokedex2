import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import _ from 'lodash';
import { GetPokemonList } from '../actions/pokemonActions';
import { Link } from 'react-router-dom';
import { Grid, makeStyles, Card, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ReactPaginate from 'react-paginate';

const useStyles = makeStyles({
  pokemonBox: {

    cursor: 'pointer',
    textAlign: 'center',
    padding: 20,
    width: '25%',
    "&:hover": {
      backgroundColor: 'lightblue',
      color: 'darkblue'
    }
  },
  mainDiv: {
    width: '90%',
    margin: 'auto',
    padding: 90
  },
  searchContainer: {
    display: 'flex',
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 5,
    marginBottom: 5,
  },
  searchIcon: {
    alignSelf:'flex-end',
    color: 'darkblue',
  },
  searchText: {
      width: 200,
      margin: 5
  }, 
  paginationStyles: {
    listStyle: 'none',
    width: 500,
    display: 'flex',
    margin: 'auto',
    justifyContent: 'space-between',
    padding: 0,
    cursor: 'pointer'
  }
});

const PokemonList = (props) => {

  const classes = useStyles();

  const [searchPokemon, setSearchPokemon] = useState("");
  const dispatch = useDispatch();
  const pokemonList = useSelector(state => state.PokemonList);

  useEffect(() => {
    FetchData(1)
  });

  const FetchData = (page = 1) => {
    dispatch(GetPokemonList(page))
  }

  const ShowData = () => {

    if (!_.isEmpty(pokemonList.data)) {
      return pokemonList.data.map(el => {
        return(
          <Grid item xs={12} sm={2} lg={12}>
            <Card className={classes.pokemonBox}>
              <p>{el.name}</p>
              <Link to={`/pokemon/${el.name}`}>View</Link>
            </Card>
          </Grid>
        )    
      })
    
    }

    if(pokemonList.loading){
      return <p>Loading Pokedex...</p>
    }

    if(pokemonList.errorMsg !== "")
    {
      return <p>{pokemonList.errorMsg}</p>
    }

    return <p>Unable to retrieve Pokemon data...</p>

  }

  return (
    <div className={classes.mainDiv}>

      <div className={classes.searchContainer}>
        <SearchIcon className={classes.searchIcon} />
        <TextField className={classes.searchText} onChange={e => {setSearchPokemon(e.target.value)}} label="Search Pokemon..." variant="standard" />
        <button onClick={() => props.history.push(`/pokemon/${searchPokemon}`)}>Search</button>
      </div>
      
      {ShowData()}
      {!_.isEmpty(pokemonList.data) && (
        <ReactPaginate 
          pageCount={Math.ceil(pokemonList.count / 20)}
          pageRangeDisplayed={2}
          marginPagesDisplayed={3}
          onPageChange={(data => FetchData(data.selected + 1))}
          className={classes.paginationStyles}
        />
      )}
    </div>
  );
}

export default PokemonList;
